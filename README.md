# Please note: this repository is not maintained anymore!
## Improved ``discretizer`` code is now [merged](https://gitlab.kwant-project.org/kwant/kwant/merge_requests/41) into [Kwant](kwant-project.org) project and will be part of 1.3 release!

I strongly encourage all user to switch to the new implementation. Development version
of kwant can be installed easily via conda with 
```
conda install -c kwant kwant
```

and new interface is briefly showed in this [notebook](http://nbviewer.jupyter.org/urls/dl.dropbox.com/s/1lx8ymybv2bt4d0/discretizer.ipynb).
Please check [tutorial](https://kwant-project.org/doc/dev/tutorial/tutorial8) as well.





# License
This code was written by Rafał Skolasiński and Sebastian Rubbert.  
The license is the same as that of Kwant (2-clause BSD): http://kwant-project.org/license
